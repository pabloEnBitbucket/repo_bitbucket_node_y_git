# utilizar imágenes que ya existen
FROM node


# directorio de trabajo
WORKDIR /apitechu

ADD . /apitechu

# defineel puerto de escucha del contenedor hacia fuera que puede ser distinto al del servicio de node
EXPOSE 3000

CMD ["npm", "start"]
