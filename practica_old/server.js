console.log("Iniciando servidro definido en server.js");

// Iniciamos el framework de express
var express = require('express');

var app = express();
var port = process.env.PORT || 3000;
app.listen(port);

// parte de express
var bodyParser = require('body-parser');
app.use(bodyParser.json());

console.log("Servidor de nodejs escuchando en puerto " + port);






app.get("/api_techu/v1",
  function(req, res) {
    console.log("GET " + "/api_techu/v1");

    res.send('{"mensaje":"Petición recibida"}' );
  }
);


app.get("/api_techu/v1/users",
  function(req, res) {
      console.log("GET " + "/api_techu/v1/users");
      res.sendFile('usuarios.json', {root: __dirname});
  }
);


app.get("/api_techu/v1.1/users",
  function(req, res) {
      console.log("GET " + "/api_techu/v1/users");
      var users = require('./usuarios.json');
      res.send(users);
  }
);

app.post("/api_techu/v1/users",
  function(req, res) {
      console.log("POST " + "/api_techu/v1/users");

      var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "country" : req.body.country
      };
      console.log("Se va a crear el usuario; " + newUser.first_name);

      var users = require('./usuarios.json');
      users.push( newUser );
      writeUserDataToFile (users);

      res.send("Usuario " + newUser + " creado");
    }
);

app.post("/api_techu/v1/monstruo",
  function(req, res) {
      console.log("POST " + "/api_techu/monstruo");
      console.log("*** Cabeceras http \n" );
      console.log( req.headers);

      console.log("*** Parámetros \n");
      console.log( req.params );

      console.log("*** Query string \n");
      console.log( req.query);

      console.log("*** Body \n");
      console.log ( req.body );

      res.send("recibido");
  }
);

app.delete("/api_techu/v1/users/:id",
  function(req, res) {
      console.log("DELETE " + "/api_techu/v1/users");
      console.log(req.params.id);

var users = require('./usuarios.json');
console.log("  ==> antes; " + users.length);
  users.splice(req.params.id - 1, 1);
console.log("  ==> después; " + users.length);
writeUserDataToFile(users);
      // req.params

     console.log("Usuario borrado");
     res.send("borrado");
  }
);

function writeUserDataToFile (data) {
    console.log("==> Entrando en writeUserDataToFile");
    // se carga en memoria com oun array y por eso se hace push
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    //var users = require('./usuarios.json');
    //users.push(data);

    fs.writeFile("./usuarios.json", jsonUserData, "utf8", function(err) {
      if(err) {
        console.log("*** eror al escribir fichero");
      } else {
        console.log("usuario persistido");
      }

    }
    );
    console.log("==> Saliendo de writeUserDataToFile");
}
