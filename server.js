console.log("Iniciando servidro definido en server.js");

// Iniciamos el framework de express
var express = require('express');

var app = express();
var port = process.env.PORT || 3000;
app.listen(port);

// parte de express
var bodyParser = require('body-parser');
app.use(bodyParser.json());

//variable para poder usar el request-json
var requestjson = require('request-json');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');
var baseMLabURL = 'https://api.mlab.com/api/1/databases/techu-5-pmc/collections/';
var apiKey = 'JfHW5FH_5fxHUyzKDobR-hIGpddEw3QW';

var json = require('request-json');
console.log("--------------------------");

console.log("Servidor de nodejs escuchando en puerto " + port);


app.get("/api_techu/v2/users",
  function(req, res) {
      console.log("GET " + "/api_techu/v2/users");

      var urlUsers = baseMLabURL + "usuario"  + "?" + "apiKey=" +  apiKey;

      console.log(urlUsers);

      var clienteMlab = requestjson.createClient(urlUsers);
        clienteMlab.get('', function(err, resM, body) {
          if(err)
          {
            console.log(err);
            res.status(500).send("Server error");
          }
          else
          {
            console.log("Datos devueltos");
            console.log(body);

            res.send(body);
          }
        });
  }
);


app.get("/api_techu/v2/users/:id",
  function(req, res) {
      console.log("GET " + "/api_techu/v2/users/:id");

      var userId = req.params.id;
      var query = 'q={"id":' + userId + '}' ;

      var urlUsers = baseMLabURL + "usuario"  + "?" + query + "&" + "apiKey=" +  apiKey;
      var url = urlUsers + "?" + query + "&" + "apiKey="+  apiKey;
      console.log(urlUsers);

      var clienteMlab = requestjson.createClient(urlUsers);
        clienteMlab.get('', function(err, resM, body) {
          if(err)
          {
            console.log(err);
            res.status(500).send("Server error");
          }
          else
          {
            if (body.length >0 ){
              console.log("Datos devueltos");
              console.log(body);
              res.send(body);
            } else {
              res.status(404).send("{}");
            }

          }
        });
  }
);



// el api de log hace un post y nosotros hacmeos un get y luego un put para actualizar el
// localhost:3000/api_techu/v2/login
// {"email" : "tmawmant@ustream.tv", "password":"Timotheus"}
app.post("/api_techu/v2/login",
  function(req, res) {
    console.log("PUT " + "/apitechu/v2/login");

    // capturamos los datos de entrada del login
    var datosEntrada = { "email" : req.body.email, "password" : req.body.password };
    console.log ("Datos introducidos; " +  datosEntrada.email + ", " + datosEntrada.password);


    var query = 'q={"email":' + '"' + datosEntrada.email    + '",'
          + '"first_name":'   + '"' + datosEntrada.password + '"'
          + '}' ;

          // primero le buscamos
    var urlUsers = baseMLabURL + "usuario"  + "?" + query + "&" + "apiKey=" +  apiKey;
    console.log("url que vamos a lanzar ----- " + urlUsers);

    var clienteMlab = requestjson.createClient(urlUsers);
      clienteMlab.get('', function(err, resM, body) {
        if(err)
        {
          console.log(body);
          res.status(500).send("Server error");
        }
        else
        {
          if (body.length >0 ){
            console.log("Datos devueltos");
            console.log(body);
            console.log("hemos encontrado al menda");


            var putBody = '{"$set":{"logged":true}}';

            urlUsers = baseMLabURL + "usuario"  + "?" + query + "&" + "apiKey=" +  apiKey;
            console.log("url que vamos a lanzar en el PPPUUUUUTTTT----- " + urlUsers);
            console.log("------------------------- " + putBody);

            clienteMlab = requestjson.createClient(urlUsers);

            clienteMlab.put('', JSON.parse(putBody), function(err, resM, body) {

              console.log("put");
              console.log(body);
              console.log(err);

              res.status(200).send("usuario logado");
            });




            //res.send(body);
          } else {
            res.status(404).send("{'error':'usuario no encontrado'}");
          }
        }
      });





    //res.send(urlUsers);

  }
);





function pruebaFuncionenServerJS () {
  console.log("*** pruebaFuncionenServerJS ***");
}

app.get("/api_techu/v1",
  function(req, res) {
    console.log("GET " + "/api_techu/v1");

    //res.send('{"mensaje":"Petición recibida"}' );
    res.send("ok");
  }
);

// Función para hacer login del usuario
/**
{
	"email": "pablo@blibli.com",
	"password": "passwordSuperChunga"
}
*/
app.post("/api_techu/v1/login",
  function(req, res) {
    console.log("POST " + "/apitechu/v1/login");

    // capturamos los datos de entrada del login
    var datosEntrada = { "email" : req.body.email, "password" : req.body.password };
    console.log ("Datos introducidos; " +  datosEntrada.email + ", " + datosEntrada.password);

    // Leemos el fichero con la lista de usuarois
    var fichero = "./usuariosPractica.json";
    var usuarios = require(fichero);
    console.log("Nº usuarios en fichero; " + usuarios.length);

    for (usuario of usuarios){
      console.log( usuario.email + ", " + datosEntrada.email);
      if( usuario.email == datosEntrada.email && ( usuario.password == datosEntrada.password )
        ){
          console.log("Login satisfactorio");
          usuario.logged = true;
          writeUserDataToFile(usuarios, fichero);
          break;
        }
    }

    if (usuario.logged == true) {
      res.send('{"id" : "' + usuario.id + '"}' );
    } else {
      res.send('{"error":"ERROR, no hemos podido hacer login"}' );
    }

  }
);


// Función para hacer logout del usuario
/*
{
	"id" : "4"
}
*/
app.post("/api_techu/v1/logout",
  function(req, res) {
    console.log("POST " + "/apitechu/v1/logout");

    // capturamos los datos de entrada del logout
    var datosEntrada = { "id" : req.body.id };
    console.log ("Datos introducidos; " +  datosEntrada.id);

    // Leemos el fichero con la lista de usuarois
    var fichero = "./usuariosPractica.json";
    var usuarios = require(fichero);
    console.log("Nº usuarios en fichero; " + usuarios.length);

    var encontrado = false;
    for (usuario of usuarios){
      console.log( usuario.id + " " + usuario.email + ", " + usuario.logged);
      if( (usuario.id == datosEntrada.id ) && ( usuario.logged == true)
        ){
          console.log("Logout satisfactorio del usuario; " + usuario.id);
          encontrado = true;
          delete usuario.logged;
          writeUserDataToFile(usuarios, fichero);
          break;
        }res.should.have.status(200);
    }

    if( encontrado ){
      res.send('{"mensaje" : "logout de usuario ' + usuario.id + ' correcto"}' );
    } else {
      res.send('{"error":"ERROR, no hemos podido hacer logout"}' );
    }

  }
);


function writeUserDataToFile (data, fichero) {
    console.log("==> Entrando en writeUserDataToFile");
    // se carga en memoria com oun array y por eso se hace push
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    //var users = require('./usuarios.json');
    //users.push(data);

console.log(jsonUserData);
    fs.writeFile(fichero, jsonUserData, "utf8", function(err) {
      if(err) {
        console.log("*** eror al escribir fichero");
      } else {
        console.log("usuario persistido");
      }

    }
    );
    console.log("==> Saliendo de writeUserDataToFile");
}

// *************************************************
app.get("/api_techu/v2/users",
 function(req, res) {
     console.log("GET " + "/api_techu/v2/users");

     var urlUsers = config.get('mlab.collections.users.endpointURL');
     var apiKey = config.get('mlab.apiKey');

     console.log(urlUsers + "?" + "apiKey="+  apiKey);

     var clienteMlab = requestjson.createClient(urlUsers + "?" + "apiKey="+  apiKey);
       clienteMlab.get('', function(err, resM, body) {
         if(err)
         {
           logger.warn(err);
           res.status(500).send("Server error");
         }
         else
         {
           logger.debug("Datos devueltos");
           logger.debug(body);

           res.send(body);
         }
       });
 }
);
// http://localhost:3000/api_techu/v2/users/2
app.get("/api_techu/v2/users/:id",
 function(req, res) {
     console.log("GET " + "/api_techu/v2/users");

     var userId = req.params.id;
     var query = 'q={"id":' + userId + '}' ;

     var urlUsers = config.get('mlab.collections.users.endpointURL');
     var apiKey = config.get('mlab.apiKey');

     var url = urlUsers + "?" + query + "&" + "apiKey="+  apiKey;

     console.log(url);

     var clienteMlab = requestjson.createClient(url);
       clienteMlab.get('', function(err, resM, body) {
         if(err)
         {
           logger.warn(err);
           res.status(500).send("Server error");
         }
         else
         {
           logger.debug("Datos devueltos");
           logger.debug(body);

           res.send(body);
         }
       });
 }
);

/*
app.get("/api_techu/v1",
  function(req, res) {
    console.log("GET " + "/api_techu/v1");

    res.send('{"mensaje":"Petición recibida"}' );
  }
);
*/
/*
app.get("/api_techu/v1/users",
  function(req, res) {
      console.log("GET " + "/api_techu/v1/users");
      res.sendFile('usuarios.json', {root: __dirname});
  }
);
*/

/*
app.get("/api_techu/v1.1/users",
  function(req, res) {
      console.log("GET " + "/api_techu/v1/users");
      var users = require('./usuarios.json');
      res.send(users);
  }
);

app.post("/api_techu/v1/users",
  function(req, res) {
      console.log("POST " + "/api_techu/v1/users");

      var newUser = {
        "first_name" : req.body.first_name,
        "last_name" : req.body.last_name,
        "email" : req.body.email,
        "country" : req.body.country
      };
      console.log("Se va a crear el usuario; " + newUser.first_name);

      var users = require('./usuarios.json');
      users.push( newUser );
      writeUserDataToFile (users);

      res.send("Usuario " + newUser + " creado");
    }
);

app.post("/api_techu/v1/monstruo",
  function(req, res) {
      console.log("POST " + "/api_techu/monstruo");
      console.log("*** Cabeceras http \n" );
      console.log( req.headers);

      console.log("*** Parámetros \n");
      console.log( req.params );

      console.log("*** Query string \n");
      console.log( req.query);

      console.log("*** Body \n");
      console.log ( req.body );

      res.send("recibido");
  }
);

app.delete("/api_techu/v1/users/:id",
  function(req, res) {
      console.log("DELETE " + "/api_techu/v1/users");
      console.log(req.params.id);

var users = require('./usuarios.json');
console.log("  ==> antes; " + users.length);
  users.splice(req.params.id - 1, 1);
console.log("  ==> después; " + users.length);
writeUserDataToFile(users);
      // req.params

     console.log("Usuario borrado");
     res.send("borrado");
  }
);


function writeUserDataToFile (data) {
    console.log("==> Entrando en writeUserDataToFile");
    // se carga en memoria com oun array y por eso se hace push
    var fs = require('fs');
    var jsonUserData = JSON.stringify(data);

    //var users = require('./usuarios.json');
    //users.push(data);

    fs.writeFile("./usuarios.json", jsonUserData, "utf8", function(err) {
      if(err) {
        console.log("*** eror al escribir fichero");
      } else {
        console.log("usuario persistido");
      }

    }
    );
    console.log("==> Saliendo de writeUserDataToFile");
}
*/
